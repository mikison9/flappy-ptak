use amethyst::core::{timing::Time, Transform};
use amethyst::derive::SystemDesc;
use amethyst::ecs::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage};
use amethyst::input::{InputHandler, StringBindings};

use crate::flappy_ptak::{
    get_barrier_y_positions, Barrier, BarrierPair, Birb, BARRIER_COUNT, BARRIER_SPACING,
    BARRIER_SPEED, BARRIER_WIDTH, BIRB_JUMP_STRENGTH, GRAVITY_STRENGTH,
};

#[derive(SystemDesc)]
pub struct PtakSystem {
    jump_locked: bool,
}

impl PtakSystem {
    pub fn default() -> Self {
        PtakSystem { jump_locked: false }
    }
}

impl<'s> System<'s> for PtakSystem {
    type SystemData = (
        WriteStorage<'s, Birb>,
        WriteStorage<'s, Transform>,
        Read<'s, InputHandler<StringBindings>>,
        ReadExpect<'s, Time>,
    );

    fn run(&mut self, (mut birbs, mut transforms, input, time): Self::SystemData) {
        for (mut birb, mut transform) in (&mut birbs, &mut transforms).join() {
            let birb_y = transform.translation().y;

            // println!("birb y: {}", birb_y);

            let birb_y = birb_y + birb.velocity_y;

            let mult = time.delta_seconds();

            // println!("mult: {}", mult);

            birb.velocity_y = birb.velocity_y - (GRAVITY_STRENGTH * mult);

            transform.set_translation_y(birb_y);

            if input.action_is_down("jump").unwrap() && !self.jump_locked {
                birb.velocity_y = BIRB_JUMP_STRENGTH;
                self.jump_locked = true;
            } else {
                self.jump_locked = false;
            }
        }
    }
}
