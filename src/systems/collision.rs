use amethyst::core::Transform;
use amethyst::derive::SystemDesc;
use amethyst::ecs::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage};
use amethyst::input::{InputHandler, StringBindings};

use crate::flappy_ptak::{
    get_barrier_y_positions, Barrier, BarrierPair, Birb, BARRIER_COUNT, BARRIER_HEIGHT,
    BARRIER_SPACING, BARRIER_SPEED, BARRIER_WIDTH, BIRB_HEIGHT, BIRB_JUMP_STRENGTH, BIRB_WIDTH,
    GAME_HEIGHT, GRAVITY_STRENGTH,
};

#[derive(SystemDesc)]
pub struct CollisionSystem;

impl<'s> System<'s> for CollisionSystem {
    type SystemData = (
        WriteStorage<'s, Birb>,
        ReadStorage<'s, Barrier>,
        ReadStorage<'s, Transform>,
    );

    fn run(&mut self, (mut birbs, barriers, transforms): Self::SystemData) {
        for (barrier, barrier_transform) in (&barriers, &transforms).join() {
            let barrier_x = barrier_transform.translation().x;
            let barrier_y = barrier_transform.translation().y;

            for (mut birb, birb_transform) in (&mut birbs, &transforms).join() {
                let birb_x = birb_transform.translation().x;
                let birb_y = birb_transform.translation().y;

                if point_in_rect(
                    birb_x,
                    birb_y,
                    barrier_x - BARRIER_WIDTH * 0.5 - BIRB_WIDTH * 0.5,
                    barrier_y - BARRIER_HEIGHT * 0.5 - BIRB_HEIGHT * 0.5,
                    barrier_x + BARRIER_WIDTH * 0.5 + BIRB_WIDTH * 0.5,
                    barrier_y + BARRIER_HEIGHT * 0.5 + BIRB_HEIGHT * 0.5,
                ) || birb_y < 0.0
                    || birb_y > GAME_HEIGHT
                {
                    birb.collided = true;
                // println!("birb count: {}", birbs.count());
                } else {
                    birb.collided = false;
                }
            }
        }
    }
}

fn point_in_rect(x: f32, y: f32, left: f32, bottom: f32, right: f32, top: f32) -> bool {
    x >= left && x <= right && y >= bottom && y <= top
}
