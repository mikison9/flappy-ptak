use amethyst::core::{timing::Time, Transform};
use amethyst::derive::SystemDesc;
use amethyst::ecs::{Join, Read, ReadExpect, ReadStorage, System, SystemData, WriteStorage};
use amethyst::input::{InputHandler, StringBindings};

use crate::flappy_ptak::{
    get_barrier_y_positions, Barrier, BarrierPair, BARRIER_COUNT, BARRIER_SPACING, BARRIER_SPEED,
    BARRIER_WIDTH,
};

#[derive(SystemDesc)]
pub struct BarrierSystem;

impl<'s> System<'s> for BarrierSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        WriteStorage<'s, Barrier>,
        WriteStorage<'s, BarrierPair>,
        ReadExpect<'s, Time>,
    );

    fn run(&mut self, (mut transforms, mut barriers, mut barrier_pairs, time): Self::SystemData) {
        //println!("{}", barrier_pairs.is_empty());
        for barrier_pair in (&mut barrier_pairs).join() {
            //println!("HALO");
            let barrier_x = transforms.get(barrier_pair.top).unwrap().translation().x;

            //println!("barrier x: {}", barrier_x);

            if barrier_x + BARRIER_WIDTH * 0.5 < 0.0 {
                //let barrier_pair_count = barrier_pairs.count();

                barrier_pair.passed = false;

                let (bottom_y, top_y) = get_barrier_y_positions();

                barrier_pair.set_translation_xy(
                    (BARRIER_COUNT) as f32 * BARRIER_SPACING,
                    bottom_y,
                    top_y,
                    &mut transforms,
                );
            } else {
                let mult = time.delta_seconds();
                barrier_pair.set_translation_x(barrier_x - BARRIER_SPEED * mult, &mut transforms);
            }
        }
    }
}
