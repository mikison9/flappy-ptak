use amethyst::core::Transform;
use amethyst::derive::SystemDesc;
use amethyst::ecs::{Join, Read, ReadStorage, System, SystemData, WriteExpect, WriteStorage};
use amethyst::input::{InputHandler, StringBindings};
use amethyst::ui::UiText;

use crate::flappy_ptak::{
    get_barrier_y_positions, Barrier, BarrierPair, Birb, Score, BARRIER_COUNT, BARRIER_SPACING,
    BARRIER_SPEED, BARRIER_WIDTH, BIRB_JUMP_STRENGTH, GAME_WIDTH, GRAVITY_STRENGTH,
};

#[derive(SystemDesc)]
pub struct ScoreSystem;

impl ScoreSystem {
    pub fn default() -> Self {
        ScoreSystem {}
    }
}

impl<'s> System<'s> for ScoreSystem {
    type SystemData = (
        ReadStorage<'s, Birb>,
        WriteStorage<'s, BarrierPair>,
        ReadStorage<'s, Transform>,
        WriteStorage<'s, UiText>,
        WriteExpect<'s, Score>,
    );

    fn run(&mut self, (birbs, mut barriers, transforms, mut texts, mut score): Self::SystemData) {
        for (birb, birb_transform) in (&birbs, &transforms).join() {
            let birb_x = birb_transform.translation().x;

            for barrier in (&mut barriers).join() {
                let barrier_x = transforms.get(barrier.top).unwrap().translation().x;

                // println!("runuje to vubec?");

                /*if (barrier_x < GAME_WIDTH * 0.5) {
                    println!("negr je za pulkou");
                }*/

                if barrier_x < birb_x && !barrier.passed {
                    barrier.passed = true;
                    score.score = score.score + 1;
                    let text = texts.get_mut(score.text);

                    match text {
                        Some(ui_text) => ui_text.text = score.score.to_string(),
                        _ => (),
                    }

                    println!("score: {}", score.score);
                }
            }
        }
    }
}
