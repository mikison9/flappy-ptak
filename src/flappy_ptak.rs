use amethyst::{
    assets::{AssetStorage, Handle, Loader},
    core::{timing::Time, transform::Transform},
    ecs::prelude::{Component, DenseVecStorage, Entity, Join, Storage, WriteStorage},
    input::{is_key_down, VirtualKeyCode},
    prelude::*,
    renderer::{Camera, ImageFormat, SpriteRender, SpriteSheet, SpriteSheetFormat, Texture},
    ui::{Anchor, FontAsset, TtfFormat, UiText, UiTransform},
};

use rand::{rngs::SmallRng, Rng, RngCore, SeedableRng};

pub const GAME_WIDTH: f32 = 100.0;
pub const GAME_HEIGHT: f32 = 100.0;

pub const BIRB_WIDTH: f32 = 4.0;
pub const BIRB_HEIGHT: f32 = 4.0;

pub const BIRB_RADIUS: f32 = 2.0;

pub const BIRB_JUMP_STRENGTH: f32 = 0.40;

pub const BARRIER_WIDTH: f32 = 4.0;
pub const BARRIER_HEIGHT: f32 = 120.0;

pub const MAX_BARRIER_OFFSET: i32 = 13;

pub const BARRIER_SPEED: f32 = 20.0;
pub const BARRIER_SPACING: f32 = 60.0;

pub const BARRIER_COUNT: u32 = 10;

pub const BARRIER_START_OFFSET: f32 = 70.0;

pub const GRAVITY_STRENGTH: f32 = 0.8;

pub const SCORE_PADDING: f32 = 50.0;

pub struct FlappyPtak {
    pub sprite_sheet: Option<Handle<SpriteSheet>>,
    pub font: Option<Handle<FontAsset>>,
}

impl FlappyPtak {
    pub fn default() -> Self {
        FlappyPtak {
            sprite_sheet: None,
            font: None,
        }
    }

    pub fn new(sprite_sheet: Option<Handle<SpriteSheet>>, font: Option<Handle<FontAsset>>) -> Self {
        FlappyPtak { sprite_sheet, font }
    }
}

impl SimpleState for FlappyPtak {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        if self.sprite_sheet.is_none() {
            self.sprite_sheet = Some(load_sprite_sheet(world));
        }

        if self.font.is_none() {
            self.font = Some(load_font(world));
        }

        world.register::<Birb>();
        world.register::<Barrier>();
        world.register::<BarrierPair>();

        initialize_camera(world);
        initialize_birb(world, self.sprite_sheet.clone().unwrap());
        initialize_barriers(world, self.sprite_sheet.clone().unwrap());
        initialize_ui(world, self.font.clone().unwrap());

        world.insert(Time::default());
    }

    fn update(&mut self, data: &mut StateData<'_, GameData<'_, '_>>) -> SimpleTrans {
        let barriers = data.world.read_storage::<Barrier>();
        let transforms = data.world.read_storage::<Transform>();
        let birbs = data.world.read_storage::<Birb>();

        for (barrier, barrier_transform) in (&barriers, &transforms).join() {
            let barrier_x = barrier_transform.translation().x;
            let barrier_y = barrier_transform.translation().y;

            for (birb, birb_transform) in (&birbs, &transforms).join() {
                let birb_x = birb_transform.translation().x;
                let birb_y = birb_transform.translation().y;

                if point_in_rect(
                    birb_x,
                    birb_y,
                    barrier_x - BARRIER_WIDTH * 0.5 - BIRB_WIDTH * 0.5,
                    barrier_y - BARRIER_HEIGHT * 0.5 - BIRB_HEIGHT * 0.5,
                    barrier_x + BARRIER_WIDTH * 0.5 + BIRB_WIDTH * 0.5,
                    barrier_y + BARRIER_HEIGHT * 0.5 + BIRB_HEIGHT * 0.5,
                ) || birb_y < 0.0
                    || birb_y > GAME_HEIGHT
                {
                    println!("KOLIZE");
                    let score = data.world.read_resource::<Score>();
                    return Trans::Replace(Box::from(ScoreScreen::new(
                        score.score,
                        self.sprite_sheet.take().unwrap(),
                        self.font.take().unwrap(),
                    )));
                    // birb.collided = true;
                    // println!("birb count: {}", birbs.count());
                }
            }
        }

        Trans::None
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        data.world.delete_all();
    }
}

fn load_sprite_sheet(world: &mut World) -> Handle<SpriteSheet> {
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "texture/spritesheet.png",
            ImageFormat::default(),
            (),
            &texture_storage,
        )
    };

    let loader = world.read_resource::<Loader>();
    let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
    loader.load(
        "texture/spritesheet.ron",
        SpriteSheetFormat(texture_handle),
        (),
        &sprite_sheet_store,
    )
}

fn load_font(world: &mut World) -> Handle<FontAsset> {
    let font = world.read_resource::<Loader>().load(
        "font/square.ttf",
        TtfFormat,
        (),
        &world.read_resource(),
    );

    font
}

fn initialize_camera(world: &mut World) {
    let mut transform = Transform::default();
    transform.set_translation_xyz(GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5, 1.0);

    world
        .create_entity()
        .with(Camera::standard_2d(GAME_WIDTH, GAME_HEIGHT))
        .with(transform)
        .build();
}
pub struct Birb {
    pub velocity_y: f32,
    pub radius: f32,
    pub collided: bool,
}

impl Component for Birb {
    type Storage = DenseVecStorage<Self>;
}

fn initialize_birb(world: &mut World, sprite_sheet: Handle<SpriteSheet>) {
    let mut transform = Transform::default();
    transform.set_translation_xyz(GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5, 0.0);

    let sprite_renderer = SpriteRender {
        sprite_sheet,
        sprite_number: 1,
    };

    world
        .create_entity()
        .with(sprite_renderer)
        .with(Birb {
            velocity_y: 0.0,
            radius: BIRB_RADIUS,
            collided: false,
        })
        .with(transform)
        .build();
}

pub struct Barrier {
    width: f32,
    height: f32,
}

impl Component for Barrier {
    type Storage = DenseVecStorage<Self>;
}

impl Barrier {
    pub fn new() -> Self {
        Barrier {
            width: BARRIER_WIDTH,
            height: BARRIER_HEIGHT,
        }
    }
}

pub struct BarrierPair {
    pub top: Entity,
    pub bottom: Entity,
    pub passed: bool,
}

impl Component for BarrierPair {
    type Storage = DenseVecStorage<Self>;
}

impl BarrierPair {
    pub fn set_translation_x(&self, x: f32, storage: &mut WriteStorage<Transform>) {
        storage.get_mut(self.top).unwrap().set_translation_x(x);
        storage.get_mut(self.bottom).unwrap().set_translation_x(x);
    }

    pub fn set_translation_xy(
        &self,
        x: f32,
        bottom_y: f32,
        top_y: f32,
        storage: &mut WriteStorage<Transform>,
    ) {
        storage
            .get_mut(self.top)
            .unwrap()
            .set_translation_xyz(x, top_y, 0.0);
        storage
            .get_mut(self.bottom)
            .unwrap()
            .set_translation_xyz(x, bottom_y, 0.0);
    }
}

fn initialize_barriers(world: &mut World, handle: Handle<SpriteSheet>) {
    let game_middle_x = GAME_WIDTH / 2.0;
    for index in 0..BARRIER_COUNT {
        let (bottom_y, top_y) = get_barrier_y_positions();

        let mut bottom_transform = Transform::default();
        bottom_transform.set_translation_xyz(
            game_middle_x + index as f32 * BARRIER_SPACING + BARRIER_START_OFFSET,
            bottom_y,
            0.0,
        );

        let mut top_transform = Transform::default();
        top_transform.set_translation_xyz(
            game_middle_x + index as f32 * BARRIER_SPACING + BARRIER_START_OFFSET,
            top_y,
            0.0,
        );

        let sprite_render = SpriteRender {
            sprite_sheet: handle.clone(),
            sprite_number: 0,
        };

        let bottom_barrier = world
            .create_entity()
            .with(bottom_transform)
            .with(Barrier::new())
            .with(sprite_render.clone())
            .build();

        let top_barrier = world
            .create_entity()
            .with(top_transform)
            .with(Barrier::new())
            .with(sprite_render)
            .build();

        world
            .create_entity()
            .with(BarrierPair {
                top: top_barrier,
                bottom: bottom_barrier,
                passed: false,
            })
            .build();
    }
}

pub fn get_barrier_y_positions() -> (f32, f32) {
    let mut small_rng = SmallRng::from_entropy();

    let offset =
        (((small_rng.next_u32() as i32) % (MAX_BARRIER_OFFSET * 2)) - MAX_BARRIER_OFFSET) as f32;

    // println!("offset: {}", offset);

    (
        BARRIER_HEIGHT * 0.5 + offset - 70.0,
        (GAME_HEIGHT - (BARRIER_HEIGHT * 0.5)) + offset + 90.0,
    )
}

fn point_in_rect(x: f32, y: f32, left: f32, bottom: f32, right: f32, top: f32) -> bool {
    x >= left && x <= right && y >= bottom && y <= top
}

pub struct Score {
    pub score: u32,
    pub text: Entity,
}

fn initialize_ui(world: &mut World, font: Handle<FontAsset>) {
    let transform = UiTransform::new(
        "score".to_string(),
        Anchor::TopMiddle,
        Anchor::TopMiddle,
        0.,
        -SCORE_PADDING,
        1.,
        200.,
        50.,
    );

    let text = world
        .create_entity()
        .with(transform)
        .with(UiText::new(font, "0".to_string(), [1., 1., 1., 1.], 50.))
        .build();

    world.insert(Score { score: 0, text });
}

struct ScoreScreen {
    score: u32,
    sprite_sheet: Option<Handle<SpriteSheet>>,
    font: Option<Handle<FontAsset>>,
}

impl ScoreScreen {
    pub fn new(score: u32, sprite_sheet: Handle<SpriteSheet>, font: Handle<FontAsset>) -> Self {
        ScoreScreen {
            score,
            sprite_sheet: Some(sprite_sheet),
            font: Some(font),
        }
    }
}

impl SimpleState for ScoreScreen {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        initialize_score_text(world, self.font.clone().unwrap(), self.score);

        println!("umrels blbecku {}", self.score);
    }

    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
        event: StateEvent,
    ) -> SimpleTrans {
        if let StateEvent::Window(event) = &event {
            if is_key_down(&event, VirtualKeyCode::Return) {
                data.world.delete_all();
                return Trans::Replace(Box::from(FlappyPtak::new(
                    self.sprite_sheet.take(),
                    self.font.take(),
                )));
            }
        }

        // Escape isn't pressed, so we stay in this `State`.
        Trans::None
    }
}

fn initialize_score_text(world: &mut World, font: Handle<FontAsset>, score: u32) {
    let transform = UiTransform::new(
        "score".to_string(),
        Anchor::Middle,
        Anchor::Middle,
        0.,
        SCORE_PADDING,
        1.,
        350.,
        50.,
    );

    world
        .create_entity()
        .with(transform)
        .with(UiText::new(
            font.clone(),
            format!("Your score: {}", score),
            [1., 1., 1., 1.],
            50.,
        ))
        .build();

    let transform = UiTransform::new(
        "text".to_string(),
        Anchor::Middle,
        Anchor::Middle,
        0.,
        -SCORE_PADDING,
        1.,
        600.,
        50.,
    );

    world
        .create_entity()
        .with(transform)
        .with(UiText::new(
            font,
            "Press enter to restart".to_string(),
            [1., 1., 1., 1.],
            50.,
        ))
        .build();
}
